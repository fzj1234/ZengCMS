<?php
// +----------------------------------------------------------------------
// | ZengCMS [ 火火 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2018 http://zengcms.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 火火 <zengcms@qq.com>
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | ThirdApp模型
// +----------------------------------------------------------------------
namespace app\api\model;

class ThirdApp extends BaseModel
{
	protected $name = 'member';
	// 错误信息
    protected $error = null;
	/**
	 * 获取访问API的各应用账号密码表信息
	 * @param  [type] $account  [账号]
	 * @param  [type] $password [密码]
	 * @return [type]     [description]
	 */
	public static function check($account, $password)
	{
		$app = self::where('username', $account)->find();
		if(!$app){
			self::$error = '账号不存在！';
			return false;
		}
		$password = encrypt_password($password, $app["encrypt"]);
		if ($password != $app['password']) {
			self::$error = '密码错误！';
			return false;
		}
		return $app;
	}
}
