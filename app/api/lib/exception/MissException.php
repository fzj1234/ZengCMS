<?php
// +----------------------------------------------------------------------
// | ZengCMS [ 火火 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2018 http://zengcms.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 火火 <zengcms@qq.com>
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | 自定义手动抛出异常处理类(Miss)
// +----------------------------------------------------------------------
namespace app\api\lib\exception;

use think\Exception;

class MissException extends Exception
{
    // 异常代码
    public $code = 0;
    // 异常消息内容
    public $message = 'global:your required resource are not found';
    // http状态码
    public $httpCode = 400;
    /**
     * 构造函数，接收一个关联数组
     * @param array $params [关联数组只应包含httpCode、message和code，且不应该是空值]
     */
    public function __construct($params = [])
    {
        // 判断是否"数组"
        if (!is_array($params)) {
            return;
        }
        // 判断是否存在"异常代码"
        if (array_key_exists('code', $params)) {
            $this->code = $params['code'];
        }
        // 判断是否存在"异常消息内容"
        if (array_key_exists('message', $params)) {
            $this->message = $params['message'];
        }
        // 判断是否存在"http状态码"
        if (array_key_exists('httpCode', $params)) {
            $this->httpCode = $params['httpCode'];
        }
    }
}
