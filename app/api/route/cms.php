<?php
// +----------------------------------------------------------------------
// | ZengCMS [ 火火 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2018 http://zengcms.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 火火 <zengcms@qq.com>
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | api(cms)路由注册
// +----------------------------------------------------------------------
use think\facade\Route;
// MISS路由开启后，默认的普通模式也将无法访问。
// 一旦设置了MISS路由，相当于开启了强制路由模式。
// 当全部路由没有匹配到时执行此路由。
/* Route::miss(function () {
    return json([
		// 异常消息代码
		'code' => 0,
		// 异常消息内容
        'message' => 'your required resource are not found'
    ],400);
}); */
Route::group('cms', function () {
	// 获取token
	Route::post('/:ver/token/app', ':ver.Token/getAppToken');
	// 验证token
	Route::post('/:ver/token/verify', ':ver.Token/verifyToken');
	// 刷新token
	Route::post('/:ver/token/refresh_token', ':ver.Token/refreshToken');
	// 首页底部资讯中心
	Route::post('/:ver/category/information_center', ':ver.Category/information_center');
	// 分类内容
	Route::post('/:ver/category/detail', ':ver.Category/detail');
	// 资讯中心右侧分类
	Route::post('/:ver/category/get_cate', ':ver.Category/get_cate');
	// 文章列表
	Route::post('/:ver/article/index', ':ver.Article/index');
	// 热门标签
	Route::post('/:ver/tag/get_hot_tag', ':ver.Tag/get_hot_tag');
	// tag文章列表
	Route::post('/:ver/tag/article', ':ver.Tag/article');
	// 文章内容
	Route::post('/:ver/article/detail', ':ver.Article/detail');
});