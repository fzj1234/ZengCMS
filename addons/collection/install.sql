DROP TABLE IF EXISTS `__PREFIX__node`;
CREATE TABLE `__PREFIX__node` (
`id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
`node_name` varchar(64) NOT NULL DEFAULT '' COMMENT '节点名称',
`model_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '所属模型ID',
`output_encoding` varchar(32) NOT NULL DEFAULT 'UTF-8' COMMENT '输出编码',
`input_encoding` varchar(32) NOT NULL DEFAULT 'GB2312' COMMENT '输入编码',
`create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
`update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
`list_rules` varchar(255) NOT NULL DEFAULT '' COMMENT '列表采集规则',
`item_rules` longtext COMMENT '内容采集规则',
`thumb_field` varchar(64) NOT NULL DEFAULT 'thumb' COMMENT '所选模型的缩略图字段名称',
`down_attachment` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否下载图片(1:是,0:否)',
`watermark` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否图片加水印(1:是,0:否)',
`status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态(1:显示,0:隐藏)',
`sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
PRIMARY KEY (`id`) USING BTREE
)ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='采集数据节点表';

DROP TABLE IF EXISTS `__PREFIX__html`;
CREATE TABLE `__PREFIX__html`(
`id` int(10) unsigned PRIMARY KEY AUTO_INCREMENT COMMENT '主键id',
`node_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '节点id',
`url` varchar(255) NOT NULL DEFAULT '' COMMENT '文档采集地址',
`litpic` varchar(255) NOT NULL DEFAULT '' COMMENT '文档缩略图地址',
`title` varchar(255) NOT NULL DEFAULT '' COMMENT '文档标题',
`result` longtext COMMENT '采集文档结果集',
`create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '采集时间',
`status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态(1:已导出,0:未导出)',
`sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序'
)ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='采集数据临时表';