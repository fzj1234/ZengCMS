<?php
namespace addons\baidupush;

use think\Addons;
use addons\baidupush\library\Push;

class Plugin extends Addons
{
    // 该插件的基础信息
    public $info = [
        'name' => 'baidupush',
        'title' => '百度推送',
        'description' => '百度推送插件，让您的网站快速收录',
        'status' => 1,
        'author' => 'ZengCMS',
        'require' => '1.0.0',
        'version' => '1.0.0',
        'website' => '',
        'images'=>'addons/baidupush/images/baidupush.jpg',
        'group'=>'',
        'is_hook'=>1,
    ];
    public $menu = [
        'is_nav' => 0,
        'menu' => [
            "title" => '百度推送',
            "title_en" => 'Baidu push',
            'name'=>'admin/cms.Baidupush/index',
            'type'=>1,
            'condition'=>'',
            "status" => 1,
            'show'=>1,
            "icon" =>'',
            'remark'=>'',
            "sort" => 50,
            'menulist' => [
                [
                    "title" => '熊掌号推送',
                    'name'=>'admin/cms.Baidupush/xiongzhang',
                    "status" => 1,
                    'show'=>0,
                    "sort" => 50,
                ],
                [
                    "title" => '百度站长推送',
                    'name'=>'admin/cms.Baidupush/zhanzhang',
                    "status" => 1,
                    'show'=>0,
                    "sort" => 50,
                ],
            ]
        ]
    ];
    /**
     * 插件安装方法
     * @return bool
     */
    public function install()
    {
        return true;
    }
    /**
     * 插件卸载方法
     * @return bool
     */
    public function uninstall()
    {
        return true;
    }
    /**
     * 插件使用方法
     * @return bool
     */
    public function enabled()
    {
        return true;
    }
    /**
     * 插件禁用方法
     * @return bool
     */
    public function disabled()
    {
        return true;
    }
    public function baidupush($params, $extra = null)
    {
        // 判断是否安装
        if(!isAddonInstall($this->name)){
            return false;
        }
        $config = getAddonConfig($this->name);
        $urls   = is_string($params) ? [$params] : $params;
        $extra  = $extra ? $extra : 'urls';
        $status = explode(',', $config['status']);
        foreach ($status as $index => $item) {
            if ($extra == 'urls' || $extra == 'append') {
                Push::connect(['type' => $item])->realtime($urls);
            } elseif ($extra == 'del' || $extra == 'delete') {
                Push::connect(['type' => $item])->delete($urls);
            }
        }
        return true;
    }
}
