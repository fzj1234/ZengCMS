<?php
return [
    'appid' => [
        'title'=> '熊掌号appid',
        'type' => 'text',
        'rule' => 'required',
        'content' => [],
        'msg'     => '',
        'tips'    => '熊掌号请前往<a href="https://ziyuan.baidu.com/xzh/home/index" target="_blank">熊掌号平台</a>获取Appid和Token',
        'ok'      => '',
        'value'   => '',
    ],
    'token' => [
        'title'=> '熊掌号token',
        'type' => 'text',
        'rule' => 'required',
        'content' => [],
        'msg'     => '',
        'tips'    => '',
        'ok'      => '',
        'value'   => '',
    ],
    'site' => [
        'title'=> '百度站长site',
        'type' => 'text',
        'rule' => 'required',
        'content' => [],
        'msg'     => '',
        'tips'    => '熊掌号请前往<a href="https://ziyuan.baidu.com/xzh/home/index" target="_blank">熊掌号平台</a>获取Appid和Token',
        'ok'      => '',
        'value'   => '',
    ],
    'sitetoken' => [
        'title'=> '百度站长token',
        'type' => 'text',
        'rule' => 'required',
        'content' => [],
        'msg'     => '',
        'tips'    => '',
        'ok'      => '',
        'value'   => '',
    ],
    'status' => [
        'title'=> '推送状态:',
        'type' => 'checkbox',
        'rule' => 'required', //使用layui表单验证方式
        'content' => [
            // 注意左边不能为0
            'xiongzhang' => '熊掌号',
            'zhanzhang' => '百度站长',
        ],
        'msg'     => '',
        'tips'    => '',
        'ok'      => '',
        'value'   => 'xiongzhang',
    ],
    'web_site_baidupush' => [
        'title'=> '自动推送:',
        'type' => 'radio',
        'rule' => 'required',
        'content' => [
            '1' => '开启',
            '0' => '关闭'
        ],
        'msg'     => '',
        'tips'    => '如果开启百度熊掌+百度站长推送，将在文章发布时自动进行推送',
        'ok'      => '',
        'value'   => 0,
    ],
];
