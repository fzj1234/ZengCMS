DROP TABLE IF EXISTS `__PREFIX__visit_day`;
CREATE TABLE `__PREFIX__visit_day` (
`id` int(10) unsigned NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT '信息ID，自增',
`ip` varchar(16) NOT NULL DEFAULT '' COMMENT 'IP地址',
`acctime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '访问时间',
`visitpage` varchar(255) NOT NULL DEFAULT '' COMMENT '受访页面',
`antepage` varchar(255) NOT NULL DEFAULT '' COMMENT '来路页面',
`cateid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '受访栏目ID',
`artid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '受访文章ID',
`browser` varchar(255) NOT NULL DEFAULT '' COMMENT '浏览器',
`address` varchar(255) NOT NULL DEFAULT '' COMMENT '访客地址',
`network` varchar(255) NOT NULL DEFAULT '' COMMENT '网络类型',
`ismobile` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否手机；1：是；0：否',
`lang` varchar(64) NOT NULL DEFAULT '' COMMENT '所属语言'
)ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='每次(条)访问的记录表';

DROP TABLE IF EXISTS `__PREFIX__visit_detail`;
CREATE TABLE `__PREFIX__visit_detail` (
`id` int(10) unsigned NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT '信息ID，自增',
`name` varchar(255) NOT NULL DEFAULT '' COMMENT '受访页面或来路地址',
`pv` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '访问次数',
`ip` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'IP量',
`alone` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '独立访客数量',
`remark` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
`type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '类型(1:关键词搜索 2:直接访问 3:来路分析)',
`cateid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '受访栏目ID',
`artid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '受访信息ID',
`engine` varchar(255) NOT NULL DEFAULT '' COMMENT '搜索引擎',
`ismobile` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否手机  0否  1 是',
`stattime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '分析日期',
`lang` varchar(64) NOT NULL DEFAULT '' COMMENT '所属语言'
)ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='每天的受访页面或来路地址即name的访问详细表';

DROP TABLE IF EXISTS `__PREFIX__visit_summary`;
CREATE TABLE `__PREFIX__visit_summary` (
`id` int(10) unsigned NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT '信息ID，自增',
`pv` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '访问次数',
`ip` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'IP量',
`alone` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '独立访客数量',
`ispc` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '电脑访问次数',
`ismobile` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '手机访问次数',
`parttime` text COMMENT '时段访问量',
`stattime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '统计日期'
)ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='每天的时间段访问记录统计表';