<?php
// +----------------------------------------------------------------------
// | Huocms [ 火火 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2018 http://huocms.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 火火 <185789392@qq.com>
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | 统计控制器
// +----------------------------------------------------------------------
namespace app\admin\controller\cms;

use think\facade\Db;
use think\facade\View;
use think\facade\Config;
use app\admin\controller\Base;
use app\common\annotation\NodeAnotation;
use app\common\annotation\ControllerAnnotation;
/**
 * @ControllerAnnotation(title="统计管理")
 * Class Stats
 * @package app\admin\controller\cms
 */
class Stats extends Base
{
    private $ntime = ''; //今天日期
    private $dtime = ''; //今天日期
    private $status = ''; //状态
    private $tab = '';  //tab 选项卡标识
    private $stime = ''; //开始时间
    private $etime = ''; //结束时间
    private $timezt = ''; //昨天日期
    private $time7 = ''; //七天开始时间
    private $time30 = ''; //30天开始时间
    private $timed = ''; //本月开始时间
    // 初始化
    protected function initialize()
    {
        // 判断插件是否安装
        $addonInfo = getAddonInfo('stats');
        if(!$addonInfo || !$addonInfo['status']){
            $this->error($addonInfo?'插件已禁用！':'插件未安装！');
        }
        //今天日期
        $ntime = date('Y-m-d');
        $dtime = strtotime(date('Y-m-d'));
        $status = input('status') ? input('status') : '1';
        $tab = input('tab') ? input('tab') : '1';
        $stime = input('stime') ? input('stime') : $ntime;
        $etime = input('etime') ? input('etime') : $ntime;
        //昨天日期
        $timezt = date("Y-m-d", strtotime("-1 day"));
        //七天开始时间
        $time7 = date("Y-m-d", strtotime("-6 day"));
        //30天开始时间
        $time30 = date("Y-m-d", strtotime("-29 day"));
        //本月开始时间
        $timed = date('Y-m-d', mktime(0, 0, 0, date('n'), 1, date('Y')));

        if ($stime) $stime = strtotime($stime);
        if ($etime) $etime = strtotime($etime);
        if ($stime > $etime) {
            //重复strtotime了
            /*$stime=strtotime($etime);
            $etime=strtotime($stime);*/
            list($etime, $stime) = array($stime, $etime);//交换两个变量值
        }
        if ($stime && $stime > $dtime) $stime = $dtime;
        if ($etime && $etime > $dtime) $etime = $dtime;

        $this->ntime = $ntime;
        $this->dtime = $dtime;
        $this->status = $status;
        $this->tab = $tab;
        $this->stime = $stime;
        $this->etime = $etime;
        $this->timezt = $timezt;
        $this->time7 = $time7;
        $this->time30 = $time30;
        $this->timed = $timed;

        parent::initialize();
    }
    /**
     * @NodeAnotation(title="统计分析")
     */
    public function index()
    {
        $this->meta_title = "统计分析";
        $this->redirect('statistics');
    }
    /**
     * @NodeAnotation(title="访问量概况")
     */
    public function statistics()
    {
        $this->dtime = strtotime(date('Y-m-d'));
        // $map['stattime'] = $this->dtime;
        $map[] = ['stattime', '=', $this->dtime];
        $visummary = Db::name('visit_summary');
        $visit = $visummary->where($map)->find();
        if (!$visit) {
            $part = '';
            for ($i = 0; $i < 24; $i++)
                $part .= '|';
            $parttime = $part;
            $sdata ['pv'] = '0';
            $sdata ['ip'] = '0';
            $sdata ['alone'] = '0';
            $sdata ['ismobile'] = '0';
            $sdata ['ispc'] = '0';
            $sdata ['parttime'] = $parttime;
            $sdata ['stattime'] = $this->dtime;
            // $visummary->insert( $sdata );
            // $sdata ['id']= $visummary->getLastInsID();
            $sdata ['id'] = $visummary->insertGetId($sdata);
            $visit = $sdata;
        }

        $summary_pv = array();
        $summary_ip = array();
        $summary_al = array();
        $timeData = array();
        $ml = date('G') - 23;
        // $zmap['stattime'] = strtotime($this->timezt);
        $zmap[] = ['stattime', '=', strtotime($this->timezt)];
        //昨天数据
        $ztdata = $visummary->where($zmap)->find();

        //取出昨天的流量数据
        if ($ml < 0) {
            $p = 24 + $ml;
            $suy = $ztdata ? explode('|', $ztdata['parttime']) : '';
            for ($i = 0; $i < 24; $i++) {
                if ($i >= $p) {
                    // $ky=explode('-',$suy[$i]);
                    $ky = explode('-', isset($suy[$i]) ? $suy[$i] : '');
                    if (!$ky || $ky[0] == '') {
                        $ky[0] = 0;
                        $ky[1] = 0;
                        $ky[2] = 0;
                    }
                    $summary_pv[$i] = $ky[0];
                    $summary_ip[$i] = $ky[1];
                    $summary_al[$i] = $ky[2];
                    $timeData[$i] = date("m-d", strtotime("-1 day")) . ' ' . $i . ':00';
                }
            }
        }
        //今天的流量数据
        $sud = $visit ? explode('|', $visit['parttime']) : '';
        for ($i = 0; $i < 24; $i++) {
            if ($i <= date('G')) {
                $kd = explode('-', $sud[$i]);
                if (!$kd || $kd[0] == '') {
                    $kd[0] = 0;
                    $kd[1] = 0;
                    $kd[2] = 0;
                }
                $summary_pv[$i] = $kd[0];
                $summary_ip[$i] = $kd[1];
                $summary_al[$i] = $kd[2];
                $timeData[$i] = date("m-d") . ' ' . $i . ':00';
            }
        }
        //昨天流量情况
        if (!$ztdata) {
            $ztdata['pv'] = 0;
            $ztdata['ip'] = 0;
            $ztdata['alone'] = 0;
            $ztdata['age'] = 0;
        }
        
        //统计数据
        $tableName = Config::get('database.connections.mysql.prefix') . 'visit_summary';
        $sql = 'SELECT sum(pv) AS sumpv ,sum(ip) AS sumip,sum(alone) AS sumalone,AVG(pv) as avgpv,AVG(ip) as avgip ,AVG(alone) as avgalone FROM' . ' ' . $tableName;
        $data = Db::query($sql);
        if ($data[0]) {
            $data = $data[0];
        }

        //每日平均值
        $dayavg['avgpv'] = round($data['avgpv'], 2);
        $dayavg['avgip'] = round($data['avgip'], 2);
        $dayavg['avgalone'] = round($data['avgalone'], 2);

        //历史最高值
        $sql = 'SELECT * FROM
		(SELECT pv AS hpv,stattime as pvtime FROM' . ' ' . $tableName . ' ' . 'ORDER BY pv DESC LIMIT 1) AS p,
		(SELECT ip AS hip,stattime as iptime FROM' . ' ' . $tableName . ' ' . 'ORDER BY ip DESC LIMIT 1) AS i,
		(SELECT alone as halone,stattime as altime FROM' . ' ' . $tableName . ' ' . 'ORDER BY alone DESC LIMIT 1) AS a;';
        $heigdata = Db::query($sql);
        if ($heigdata[0]) {
            $heigdata = $heigdata[0];
        }

        //历史最高纪录
        $hist_height['heigpv'] = $heigdata['hpv'] . '  (' . date('Y-m-d', $heigdata['pvtime']) . ')';
        $hist_height['heigip'] = $heigdata['hip'] . '  (' . date('Y-m-d', $heigdata['iptime']) . ')';
        $hist_height['heigalone'] = $heigdata['halone'] . '  (' . date('Y-m-d', $heigdata['altime']) . ')';

        //历史累计流量
        $hist_count['sumpv'] = round($data['sumpv'], 2);
        $hist_count['sumip'] = round($data['sumip'], 2);
        $hist_count['sumalone'] = round($data['sumalone'], 2);

        $visit['sumpv'] = $data['sumpv'];
        
        //今天平均值
        if ($visit['alone'] == 0) {
            $visit['avg'] = 0;
        } else {
            $visit['avg'] = round($visit['pv'] / $visit['alone'], 2);
        }

        //昨天平均值
        if ($ztdata['alone'] == 0) {
            $ztdata['avg'] = 0;
        } else {
            $ztdata['avg'] = round($ztdata['pv'] / $ztdata['alone'], 2);
        }

        //访问来源
        $cardata = $this->get_engine_fromdata($this->dtime, $this->dtime);
        $echdata = $cardata['echdata'];

        //图表数据
        // $sum_pv = str_replace('"', '', json_encode(array_values($summary_pv)));
        // $sum_ip = str_replace('"', '', json_encode(array_values($summary_ip)));
        // $sum_al = str_replace('"', '', json_encode(array_values($summary_al)));
        View::assign('sum_pv', json_encode(array_values($summary_pv)));
        View::assign('sum_ip', json_encode(array_values($summary_ip)));
        View::assign('sum_al', json_encode(array_values($summary_al)));
        View::assign('timeData', json_encode(array_values($timeData), JSON_UNESCAPED_UNICODE));

        View::assign('visit', $visit);
        View::assign('yvisit', $ztdata);
        View::assign('dayavg', $dayavg);
        View::assign('echdata', $echdata);
        View::assign('histheight', $hist_height);
        View::assign('histcount', $hist_count);
        // $this->meta_title = "访问量概况";
        View::assign('meta_title', '访问量概况');
        // $this->display();
        return view();
    }
    /**
     * @NodeAnotation(title="全部访问量")
     */
    public function allstats()
    {
        // $map['stattime']  = array('BETWEEN',array($this->stime,$this->etime));
        $map[] = ['stattime', 'between', [$this->stime, $this->etime]];
        $visummary = Db::name('visit_summary');
        $visit = $visummary->where($map)->select()->toArray();
        $edate['sumpv'] = array();
        $edate['sumip'] = array();
        $edate['sumal'] = array();
        $edate['dtime'] = array();
        $title['t1'] = '日访问量概况';
        $title['t2'] = '日访问量分布';
        if ($visit) {
            $data = array();
            foreach ($visit as $key => $vl) {
                //被除数不能为0
                if ($vl['pv'] == 0 || $vl['alone'] == 0) {
                    $vl['avg'] = 0;
                } else {
                    $vl['avg'] = round($vl['pv'] / $vl['alone'], 2);
                }
                if ($this->status == 1 || $this->status == 2) {
                    //今天的流量数据
                    $sud = $vl ? explode('|', $vl['parttime']) : '';
                    for ($i = 0; $i < 24; $i++) {
                        $kd = explode('-', $sud[$i]);
                        if (!$kd || $kd[0] == '') {
                            $kd[0] = 0;
                            $kd[1] = 0;
                            $kd[2] = 0;
                        }
                        $edate['sumpv'][$i] = $kd[0];
                        $edate['sumip'][$i] = $kd[1];
                        $edate['sumal'][$i] = $kd[2];
                        $edate['dtime'][$i] = $i . ':00';
                    }
                    $title['t1'] = '日访问量概况  (从' . date('Y-m-d', $this->stime) . ' 到 ' . date('Y-m-d', $this->etime) . ')';
                    $title['t2'] = '时间段分布  (从 0：00  到  23：00)';
                } else {
                    $edate['sumpv'][$key] = $vl['pv'];
                    $edate['sumip'][$key] = $vl['ip'];
                    $edate['sumal'][$key] = $vl['alone'];
                    $edate['dtime'][$key] = date('Y-m-d', $vl['stattime']);
                    $title['t1'] = '日访问量分布  (从' . date('Y-m-d', $this->stime) . ' 到 ' . date('Y-m-d', $this->etime) . ')';
                    $title['t2'] = '日访问量概况';
                }
                $data[] = $vl;
            }
            $visit = $data;
        }
        $edate['sumpv'] = json_encode($edate['sumpv']);
        $edate['sumip'] = json_encode($edate['sumip']);
        $edate['sumal'] = json_encode($edate['sumal']);
        $edate['dtime'] = json_encode($edate['dtime']);

        $dataUrl['time1'] = url('allstats', array('stime' => $this->ntime, 'etime' => $this->ntime, 'status' => 1));
        $dataUrl['timezt'] = url('allstats', array('stime' => $this->timezt, 'etime' => $this->timezt, 'status' => 2));
        $dataUrl['time7'] = url('allstats', array('stime' => $this->time7, 'etime' => $this->ntime, 'status' => 3));
        $dataUrl['time30'] = url('allstats', array('stime' => $this->time30, 'etime' => $this->ntime, 'status' => 4));
        $dataUrl['timed'] = url('allstats', array('stime' => $this->timed, 'etime' => $this->ntime, 'status' => 5));
        View::assign('stime', date('Y-m-d', $this->stime));
        View::assign('etime', date('Y-m-d', $this->etime));
        View::assign('status', $this->status);
        View::assign('visit', array_reverse($visit));
        View::assign('dataurl', $dataUrl);
        // print_r($edate);die;
        View::assign('edata', $edate);
        View::assign('title', $title);
        View::assign('meta_title', '查看全部');
        View::assign('st', '');//注意是我加的
        return view();
    }
    /**
     * @NodeAnotation(title="来源搜索引擎分析")
     */
    public function search_engine()
    {
        $kdata = array();
        $echdata = array();
        // $map['type'] = '1';
        $map[] = ['type', '=', '1'];
        // $map['stattime']  = array('BETWEEN',array($this->stime,$this->etime));
        $map[] = ['stattime', 'between', [$this->stime, $this->etime]];
        if ($this->tab == 1) {
            // $order = array('pv desc');
            $order = 'pv desc';
            // $group = 'name';//原来的
            $group = 'name,id,engine';//自己修改后的
            $field = 'id,name,SUM(pv) as pv,SUM(ip) as ip,SUM(alone) as alone,engine';
            // $kdata = $this->page_lists('visit_detail',$map,$order,'',$field,$group);

            /*$kdata = Db::name('visit_detail')->field($field)->where($map)->order($order)->group($group)->paginate(sys_config('WEB_ONE_PAGE_NUMBER'));
			if ($kdata) {
				$data = array();
				foreach ($kdata as $key=>$vl){
					$vl['avg'] = round($vl['pv']/$vl['alone'],2);
					$data[] = $vl;
				}
				$kdata = $data;
			}*/
            $kdata = Db::name('visit_detail')->field($field)->where($map)->order($order)->group($group)->paginate(sys_config('WEB_ONE_PAGE_NUMBER'), false, ['query' => request()->param()])->each(function ($item, $key) {
                $item['avg'] = round($item['pv'] / $item['alone'], 2);
                $data[] = $item;
                return $item;
            });
        } else {
            //搜索引擎数据
            $cardata = $this->get_engine_fromdata($this->stime, $this->etime);
            $kdata = $cardata['kdata'];
            $echdata = $cardata['echdata'];
        }
        $title['t1'] = '从 ' . date('Y-m-d', $this->stime) . ' 到 ' . date('Y-m-d', $this->etime);
        $dataUrl['time1'] = url('search_engine', array('stime' => $this->ntime, 'etime' => $this->ntime, 'status' => 1, 'tab' => $this->tab));
        $dataUrl['timezt'] = url('search_engine', array('stime' => $this->timezt, 'etime' => $this->timezt, 'status' => 2, 'tab' => $this->tab));
        $dataUrl['time7'] = url('search_engine', array('stime' => $this->time7, 'etime' => $this->ntime, 'status' => 3, 'tab' => $this->tab));
        $dataUrl['time30'] = url('search_engine', array('stime' => $this->time30, 'etime' => $this->ntime, 'status' => 4, 'tab' => $this->tab));
        $dataUrl['timed'] = url('search_engine', array('stime' => $this->timed, 'etime' => $this->ntime, 'status' => 5, 'tab' => $this->tab));
        View::assign('stime', date('Y-m-d', $this->stime));
        View::assign('etime', date('Y-m-d', $this->etime));
        View::assign('dataurl', $dataUrl);
        View::assign('echdata', $echdata);
        View::assign('title', $title);
        View::assign('kdata', $kdata);
        View::assign('status', $this->status);
        View::assign('pk', 0);//自己设置的
        // $this->meta_title = "搜索引擎";
        View::assign('meta_title', "搜索引擎");
        if ($this->tab == 2) {
            // $this->display('search_engine2');
            return view('search_engine2');
            exit();
        }
        // $this->display();
        return view('search_engine');
    }
    /**
     * @NodeAnotation(title="统计搜索引擎来源数据")
     * @param string $stime 开始时间戳
     * @param string $etime 结束时间戳
     * @return array 统计数据和图表数据
     */
    public function get_engine_fromdata($stime = '', $etime = '')
    {
        // $map['type'] = '1';
        // $map['stattime']  = array('BETWEEN',array($this->stime,$this->etime));
        $map[] = ['type', '=', '1'];
        $map[] = ['stattime', 'between', [$this->stime, $this->etime]];
        $visdetail = Db::name('visit_detail');
        $kdata = $visdetail->where($map)->field('engine')->select()->toArray();
        $enginelist = array();
        $echdata = array();
        if ($kdata) {
            foreach ($kdata as $vl) {
                $shp = explode('|', $vl['engine']);
                for ($i = 0; $i < count($shp); $i++) {
                    if ($shp[$i] != '') {
                        $kp = explode('-', $shp[$i]);
                        $stype = 's' . $kp[0];
                        $enginelist[$stype]['pv'] = $enginelist[$stype]['pv'] + $kp[1];
                    }
                }
            }
            array_multisort($enginelist, SORT_DESC);
            $arr = array();
            foreach ($enginelist as $key => $val) {
                $str = array();
                $engine = str_replace("s", "", $key);
                $ename = get_engine_type($engine);
                $str['value'] = $val['pv'];
                $str['name'] = $ename['name'];
                $arr[] = $str;
            }
            $echdata['edata'] = json_encode($arr);
            $echdata['etitle'] = json_encode(array_column($arr, 'name'));
            $kdata = $arr;
        } else {
            $echdata['etitle'] = json_encode(array('暂无数据'));
            $echdata['edata'] = json_encode(array(array('value' => 0, 'name' => '暂无数据')));
        }
        // var_dump($echdata);die;
        //array(2) { ["etitle"]=> string(28) "["\u6682\u65e0\u6570\u636e"]" ["edata"]=> string(47) "[{"value":0,"name":"\u6682\u65e0\u6570\u636e"}]" }
        // $echdata['etitle'] = str_replace('"', '\'', $echdata['etitle']); //自己加上去的 百度echarts奇葩不能含""
        // $echdata['edata'] = str_replace('"', '\'', $echdata['edata']);//自己加上去的 百度echarts奇葩不能含""
        // var_dump($echdata);die;
        // array(2) { ["etitle"]=> string(28) "['\u6682\u65e0\u6570\u636e']" ["edata"]=> string(47) "[{'value':0,'name':'\u6682\u65e0\u6570\u636e'}]" }
        return array('kdata' => $kdata, "echdata" => $echdata);
    }
    /**
     * @NodeAnotation(title="来访分析")
     */
    public function source()
    {
        // $map['type'] = array('in','2,3');
        $map[] = ['type', 'in', '2,3'];
        // $map['stattime']  = array('BETWEEN',array($this->stime,$this->etime));
        $map[] = ['stattime', 'between', [$this->stime, $this->etime]];
        // $order = array('pv desc');
        $order = 'pv desc';
        // $group = 'name';//原来
        $group = 'name,engine,remark,type,cateid,artid';//自己修改后的
        $field = 'name,SUM(pv) as pv,SUM(ip) as ip,SUM(alone) as alone,engine,remark,type,cateid,artid';
        // $sdata = $this->page_lists('visit_detail',$map,$order,'',$field,$group);
        // $sdata = Db::name('visit_detail')->field($field)->where($map)->order($order)->group($group)->paginate(sys_config('WEB_ONE_PAGE_NUMBER')); //到时再分页

        /*$sdata = Db::name('visit_detail')->field($field)->where($map)->order($order)->group($group)->select()->toArray();
        if ($sdata) {
            $data = array();
            foreach ($sdata as $key=>$vl){
                $vl['avg'] = round($vl['pv']/$vl['alone'],2);
                if (!empty($vl['name'])) {
                    $tname = '';
                    $city = '';
                    if (!empty($vl['artid'])) {
                        $tname = get_article_title($vl['artid']);
                    }elseif (!empty($vl['cateid'])) {
                        $tname = get_category_title($vl['cateid']);
                    }else {
                        $tname = $vl['name'];
                    }
                    if ($vl['remark']) {
                        $city = get_city_bypinyin($vl['remark']);
                        if ($city['shortname']) {
                            $city = ' 【 '.$city['shortname'].' 】 ';
                        }
                    }
                    $vl['name'] = '<a href="'.$vl['name'].'" target="_blank">'.$city.$tname.'</a>';
                }
                if (empty($vl['name'])) {
                    $vl['name'] = '未知访问';
                }
                $data[] = $vl;
            }
            $sdata = $data;
        }*/
        $sdata = Db::name('visit_detail')->field($field)->where($map)->order($order)->group($group)->paginate(sys_config('WEB_ONE_PAGE_NUMBER'), false, ['query' => request()->param()])->each(function ($item, $key) {
            $item['avg'] = round($item['pv'] / $item['alone'], 2);
            if (!empty($item['name'])) {
                $tname = '';
                $city = '';
                if (!empty($item['artid'])) {
                    $tname = get_article_title($item['artid']);
                } elseif (!empty($item['cateid'])) {
                    $tname = get_category_title($item['cateid']);
                } else {
                    $tname = $item['name'];
                }
                if ($item['remark']) {
                    $city = get_city_bypinyin($item['remark']);
                    if ($city && $city['shortname']) {
                        $city = ' 【 ' . $city['shortname'] . ' 】 ';
                    }
                }
                $item['name'] = '<a href="' . $item['name'] . '" target="_blank">' . $city . $tname . '</a>';
            }
            if (empty($item['name'])) {
                $item['name'] = '未知访问';
            }
            return $item;
        });

        $title['t1'] = '从 ' . date('Y-m-d', $this->stime) . ' 到 ' . date('Y-m-d', $this->etime);
        $dataUrl['time1'] = url('source', array('stime' => $this->ntime, 'etime' => $this->ntime, 'status' => 1));
        $dataUrl['timezt'] = url('source', array('stime' => $this->timezt, 'etime' => $this->timezt, 'status' => 2));
        $dataUrl['time7'] = url('source', array('stime' => $this->time7, 'etime' => $this->ntime, 'status' => 3));
        $dataUrl['time30'] = url('source', array('stime' => $this->time30, 'etime' => $this->ntime, 'status' => 4));
        $dataUrl['timed'] = url('source', array('stime' => $this->timed, 'etime' => $this->ntime, 'status' => 5));

        View::assign('stime', date('Y-m-d', $this->stime));
        View::assign('etime', date('Y-m-d', $this->etime));
        View::assign('dataurl', $dataUrl);
        View::assign('status', $this->status);
        View::assign('title', $title);
        View::assign('sdata', $sdata);
        View::assign('pk', 0);//自己设置的
        // $this->meta_title = "访问页面";
        View::assign('meta_title', "访问页面");
        return view();
    }
    /**
     * @NodeAnotation(title="来路分析")
     */
    public function ante_page()
    {
        $title['t1'] = ' 从 ' . date('Y-m-d', $this->stime) . ' 到 ' . date('Y-m-d', $this->etime);
        $dataUrl['time1'] = url('ante_page', array('stime' => $this->ntime, 'etime' => $this->ntime, 'status' => 1));
        $dataUrl['timezt'] = url('ante_page', array('stime' => $this->timezt, 'etime' => $this->timezt, 'status' => 2));
        $dataUrl['time7'] = url('ante_page', array('stime' => $this->time7, 'etime' => $this->ntime, 'status' => 3));
        $dataUrl['time30'] = url('ante_page', array('stime' => $this->time30, 'etime' => $this->ntime, 'status' => 4));
        $dataUrl['timed'] = url('ante_page', array('stime' => $this->timed, 'etime' => $this->ntime, 'status' => 5));

        View::assign('stime', date('Y-m-d', $this->stime));
        View::assign('etime', date('Y-m-d', $this->etime));
        View::assign('dataurl', $dataUrl);
        View::assign('status', $this->status);
        View::assign('title', $title);
        View::assign('pk', 0);//自己设置的
        View::assign('kdata', '');//自己设置的
        // $this->meta_title = "来路分析";
        View::assign('meta_title', "来路分析");
        return view();
    }
    /**
     * @NodeAnotation(title="访问明细")
     */
    public function details()
    {
        $title['t1'] = '访问明细  从 ' . date('Y-m-d', $this->stime) . ' 到 ' . date('Y-m-d', $this->etime);
        $dataUrl['time1'] = url('details', array('stime' => $this->ntime, 'etime' => $this->ntime, 'status' => 1));
        $dataUrl['timezt'] = url('details', array('stime' => $this->timezt, 'etime' => $this->timezt, 'status' => 2));
        $dataUrl['time7'] = url('details', array('stime' => $this->time7, 'etime' => $this->ntime, 'status' => 3));
        $dataUrl['time30'] = url('details', array('stime' => $this->time30, 'etime' => $this->ntime, 'status' => 4));
        $dataUrl['timed'] = url('details', array('stime' => $this->timed, 'etime' => $this->ntime, 'status' => 5));
        $this->etime = $this->etime + 86399;
        // $map['acctime']  = array('BETWEEN',array($this->stime,$this->etime));
        $map[] = ['acctime', 'between', [$this->stime, $this->etime]];
        $order = 'acctime desc';
        // $vdata = $this->page_lists('visit_day',$map,$order);
        // $vdata = Db::name('visit_day')->where($map)->order($order)->paginate(sys_config('WEB_ONE_PAGE_NUMBER')); //后面再做分页

        /*$vdata = Db::name('visit_day')->where($map)->order($order)->select()->toArray();
        if ($vdata) {
            $data = array();
            foreach ($vdata as $key=>$vl){
                if (!empty($vl['visitpage'])) {
                    $tname = '';
                    $city = '';
                    if (!empty($vl['artid'])) {
                        $tname = get_article_title($vl['artid']);
                    }elseif (!empty($vl['cateid'])) {
                        $tname = get_category_title($vl['cateid']);
                    }else {
                        $tname = $vl['visitpage'];
                    }
                    $vl['visitpage'] = '<a href="'.$vl['visitpage'].'" target="_blank">'.$tname.'</a>';
                }
                if (!empty($vl['antepage'])) {
                    $vl['antepage'] = '<a href="'.$vl['antepage'].'" target="_blank">'.$vl['antepage'].'</a>';
                }
                if (empty($vl['antepage'])) {
                    $vl['antepage'] = '直接访问';
                }
                if ($vl['ismobile'] ==1) {
                    $vl['ismobile'] = '【手机访问】';
                }else {
                    $vl['ismobile'] = '【电脑访问】';
                }
                $data[] = $vl;
            }
            $vdata = $data;
        }*/

        $vdata = Db::name('visit_day')->where($map)->order($order)->paginate(sys_config('WEB_ONE_PAGE_NUMBER'), false, ['query' => request()->param()])->each(function ($item, $key) {
            if (!empty($item['visitpage'])) {
                $tname = '';
                $city = '';
                if (!empty($item['artid'])) {
                    $tname = get_article_title($item['artid'], $item['cateid']);
                } elseif (!empty($item['cateid'])) {
                    $tname = get_category_title($item['cateid']);
                } else {
                    $tname = $item['visitpage'];
                }
                $item['visitpage'] = '<a href="' . $item['visitpage'] . '" target="_blank">' . $tname . '</a>';
            }
            if (!empty($item['antepage'])) {
                $item['antepage'] = '<a href="' . $item['antepage'] . '" target="_blank">' . $item['antepage'] . '</a>';
            }
            if (empty($item['antepage'])) {
                $item['antepage'] = '直接访问';
            }
            if ($item['ismobile'] == 1) {
                $item['ismobile'] = '【手机访问】';
            } else {
                $item['ismobile'] = '【电脑访问】';
            }
            return $item;
        });
        View::assign('stime', date('Y-m-d', $this->stime));
        View::assign('etime', date('Y-m-d', $this->etime));
        View::assign('dataurl', $dataUrl);
        View::assign('status', $this->status);
        View::assign('title', $title);
        View::assign('vdata', $vdata);
        View::assign('pk', 0);//自己设置的，后面再研究
        // $this->meta_title = "访问明细";
        View::assign('meta_title', "访问明细");
        return view();
    }
}
?>