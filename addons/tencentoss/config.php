<?php
return [
    'bucket' => [
        'title'=> 'bucket:',
        'type' => 'text',
        'rule' => 'required',
        'content' => [],
        'msg'     => '',
        'tips'    => '',
        'ok'      => '',
        'value'   => '',
    ],
    'APPID' => [
        'title'=> 'APPID:',
        'type' => 'text',
        'rule' => 'required',
        'content' => [],
        'msg'     => '',
        'tips'    => '',
        'ok'      => '',
        'value'   => '',
    ],
    'SecretId' => [
        'title'=> 'SecretId:',
        'type' => 'text',
        'rule' => 'required',
        'content' => [],
        'msg'     => '',
        'tips'    => '',
        'ok'      => '',
        'value'   => '',
    ],
    'SecretKey' => [
        'title'=> 'SecretKey:',
        'type' => 'text',
        'rule' => 'required',
        'content' => [],
        'msg'     => '',
        'tips'    => '',
        'ok'      => '',
        'value'   => '',
    ],
    'region' => [
        'title'=> 'region:',
        'type' => 'text',
        'rule' => 'required',
        'content' => [],
        'msg'     => '',
        'tips'    => '',
        'ok'      => '',
        'value'   => '',
    ],
];
